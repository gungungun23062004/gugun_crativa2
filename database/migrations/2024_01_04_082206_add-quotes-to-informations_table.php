<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('informations', function (Blueprint $table) {
            $table->string('quotes', 110)->after('homeBannerImage');
            $table->string('quotesBannerImage')->after('quotes');
            $table->string('persuasive', 60)->after('quotesBannerImage');
            $table->string('descPersuasive', 110)->after('persuasive');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('informations', function($table) {
            $table->dropColumn('quotes');
            $table->dropColumn('quotesBannerImage');
            $table->dropColumn('persuasive');
            $table->dropColumn('descPersuasive');
        });
    }
};
