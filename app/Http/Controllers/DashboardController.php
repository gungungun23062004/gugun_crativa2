<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\informations;
use App\Models\Contact;
use App\Models\Features;
use App\Models\Price;
use App\Models\Privilege;
use App\Models\PricePrivilege;
use Illuminate\Validation\Rule;
use Session;
use Validator;

class DashboardController extends Controller
{
    public function index()
    {
        return view('admin.dashboard.main');
    }

    public function homebanner()
    {
        $informations = informations::all();

        return view('admin.dashboard.readUpdate.home-banner', compact('informations'));
    }

    // Read Update

    public function homequotes()
    {
        $informations = informations::all();

        return view('admin.dashboard.readUpdate.home-quotes', compact('informations'));
    }
    public function homepersuasive()
    {
        $informations = informations::all();

        return view('admin.dashboard.readUpdate.home-persuasive', compact('informations'));
    }

    public function homecontacts()
    {
        $contacts = Contact::all();
        return view('admin.dashboard.readUpdate.home-contacts', compact('contacts'));
    }

    // End Read Update


    // features

    public function features()
    {
        $features = Features::all();
        return view('admin.dashboard.crud.features', compact('features'));
    }

    public function createFeatures()
    {
        $features = Features::all();
        return view('admin.dashboard.crud.create-feature', compact('features'));
    }

    public function storeFeature(Request $request)
    {
        $request->validate([
            'feature' => 'required|string',
            'desc' => 'required|string',
        ]);

        Features::create([
            'feature' => $request->input('feature'),
            'desc' => $request->input('desc'),
        ]);

        return redirect('/dashboard/features')->with('success', 'Feature created successfully');
    }

    public function editFeatures($id)
    {
        $feature = Features::find($id);
        return view('admin.dashboard.crud.edit-feature', compact('feature'));
    }

    public function updateFeature(Request $request, $id)
    {
        $request->validate([
            'feature' => 'required|string',
            'desc' => 'required|string',
        ]);

        $feature = Features::findOrFail($id);

        $feature->update([
            'feature' => $request->input('feature'),
            'desc' => $request->input('desc'),
        ]);

        return redirect('/dashboard/features')->with('success', 'Feature updated successfully');
    }


    public function featuresdelete($id)
    {
        $feature = Features::find($id);
        $feature->delete();

        return redirect('/dashboard/features');
    }

    // endfeatures

    // pricing 
    public function pricing()
    {
        $price = Price::all();
        $privilege = Privilege::all();
        $priceprivilege = PricePrivilege::orderBy('price_id')->get();
        return view('admin.dashboard.crud.pricing', compact('price', 'privilege', 'priceprivilege'));
    }

    // price

    public function createPrice()
    {
        return view('admin.dashboard.crud.create-price');
    }

    public function storePrice(Request $request)
    {
        $request->validate([
            'type' => 'required|string',
            'price_per_month' => 'required|integer',
        ]);

        Price::create([
            'type' => $request->input('type'),
            'price_per_month' => $request->input('price_per_month'),
        ]);

        return redirect('/dashboard/pricing')->with('success', 'Price created successfully');
    }

    public function editPrice($id)
    {
        $price = Price::find($id);
        return view('admin.dashboard.crud.edit-price', compact('price'));
    }

    public function updatePrice(Request $request, $id)
    {

        $request->validate([
            'type' => 'required|string',
            'price_per_month' => 'required|integer',
        ]);

        $price = Price::findOrFail($id);

        $price->update([
            'type' => $request->input('type'),
            'price_per_month' => $request->input('price_per_month'),
        ]);

        return redirect('/dashboard/pricing')->with('success', 'Price updated successfully');
    }


    public function deletePrice($id)
    {
        $price = Price::find($id);
        $price->delete();

        return redirect('/dashboard/pricing');
    }
    // end price


    // privilege
    public function createPrivilege()
    {
        return view('admin.dashboard.crud.create-privilege');
    }

    public function storePrivilege(Request $request)
    {
        $request->validate([
            'privilege' => 'required|string',
        ]);

        Privilege::create([
            'privilege' => $request->input('privilege'),
        ]);

        return redirect('/dashboard/pricing')->with('success', 'Privilege created successfully');
    }

    public function editPrivilege($id)
    {
        $privilege = Privilege::find($id);
        return view('admin.dashboard.crud.edit-privilege', compact('privilege'));
    }

    public function updatePrivilege(Request $request, $id)
    {

        $request->validate([
            'privilege' => 'required|string',
        ]);

        $privilege = Privilege::findOrFail($id);

        $privilege->update([
            'privilege' => $request->input('privilege'),
        ]);

        return redirect('/dashboard/pricing')->with('success', 'Privilege updated successfully');
    }


    public function deletePrivilege($id)
    {
        $privilege = Privilege::find($id);
        $privilege->delete();

        return redirect('/dashboard/pricing');
    }

    // end privilege



    public function createPricePrivilege()
    {
        $price = Price::all();
        $privilege = Privilege::all();
        return view('admin.dashboard.crud.create-price-privilege', compact('price', 'privilege'));
    }

    public function storePricePrivilege(Request $request)
    {
        $request->validate([
            'type' => 'required|int|exists:prices,id',
            'privilege' => 'required|int|exists:privileges,id',
        ]);

        $validator = Validator::make($request->all(), [
            'type' => Rule::unique('price_privilege', 'price_id')
                ->where('privilege_id', $request->input('privilege')),
            'privilege' => Rule::unique('price_privilege', 'privilege_id')
                ->where('price_id', $request->input('type')),
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->with('error', 'Juntion already exists. ')
                ->withInput();
        }

        PricePrivilege::create([
            'price_id' => $request->input('type'),
            'privilege_id' => $request->input('privilege'),
        ]);

        return redirect('/dashboard/pricing')->with('success', 'Junction created successfully');
    }


    public function editPricePrivilege($id)
    {
        $price = Price::all();
        $privilege = Privilege::all();
        $priceprivilege = PricePrivilege::find($id);
        return view('admin.dashboard.crud.edit-price-privilege', compact('price', 'privilege', 'priceprivilege'));
    }

    public function updatePricePrivilege(Request $request, $id)
    {
        $request->validate([
            'type' => 'required|int',
            'privilege' => 'required|int',
        ]);

        $priceprivilege = PricePrivilege::findOrFail($id);

        $priceprivilege->update([
            'price_id' => $request->input('type'),
            'privilege_id' => $request->input('privilege'),
        ]);

        return redirect('/dashboard/pricing')->with('success', 'Junction updated successfully');
    }

    public function deletePricePrivilege($id)
    {
        $priceprivilege = Priceprivilege::find($id);
        $priceprivilege->delete();

        return redirect('/dashboard/pricing');
    }
}
