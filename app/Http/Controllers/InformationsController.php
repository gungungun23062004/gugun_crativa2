<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\Models\Contact;
use App\Models\informations;
use Illuminate\Http\Request;

class InformationsController extends Controller
{
    public function updateHomeBanner(Request $request, $id)
    {
        $information = informations::findOrFail($id);

        $information->update([
            'catalogue' => $request->input('catalogue'),
            'desc' => $request->input('desc'),
        ]);


        $information = informations::findOrFail($id);
        if ($request->hasFile('image')) {

            if ($information->homeBannerImage) {
                unlink(public_path('assets/img/home/' . $information->homeBannerImage));
            }

            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('assets/img/home'), $imageName);

            $information->update(['homeBannerImage' => $imageName]);
        }


        return redirect()->back()->with('success', 'Information updated successfully');
    }

    public function updateQuotes(Request $request, $id)
    {
        $information = informations::findOrFail($id);

        $information->update([
            'quotes' => $request->input('quotes'),
        ]);

        $information = informations::findOrFail($id);
        if ($request->hasFile('imageQuote')) {

            if ($information->quotesBannerImage) {
                unlink(public_path('assets/img/home/' . $information->quotesBannerImage));
            }

            $image = $request->file('imageQuote');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('assets/img/home'), $imageName);

            $information->update(['quotesBannerImage' => $imageName]);
        }

        return redirect()->back()->with('success', 'quote updated successfully');
    }

    public function updatePersuasive(Request $request, $id)
    {
        $information = informations::findOrFail($id);

        $information->update([
            'persuasive' => $request->input('persuasive'),
            'descPersuasive' => $request->input('descPersuasive'),
        ]);

        return redirect()->back()->with('success', 'persuasive updated successfully');
    }

    public function updateContacts(Request $request, $id)
    {
        $contacts = Contact::findOrFail($id);

        $contacts->update([
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'location' => $request->input('location'),
        ]);

        return redirect()->back()->with('success', 'contact updated successfully');
    }
}
