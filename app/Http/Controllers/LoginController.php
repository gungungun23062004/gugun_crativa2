<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class LoginController extends Controller
{
    public function signup()
    {
        if (Auth::check()) {
            return redirect('dashboard');
        } else {
            return view('auth-signUp');
        }
    }

    public function actionlogin(Request $request)
    {
        $data = [
            'username' => $request->input('username'),
            'password' => $request->input('password'),
        ];

        if (Auth::Attempt($data)) {
            return redirect('dashboard');
        } else {
            Session::flash('error', 'username atau Password Salah');
            return redirect('/signUp');
        }
    }

    public function actionlogout()
    {
        Auth::logout();
        return redirect('/signUp');
    }
}
