<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Privilege extends Model
{
    use HasFactory;
    protected $fillable = ['privilege'];

    public function prices()
    {
        return $this->belongsToMany(Price::class, 'price_privilege', 'privilege_id', 'price_id');
    }
}
