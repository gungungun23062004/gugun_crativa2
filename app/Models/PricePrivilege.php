<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PricePrivilege extends Model
{
    use HasFactory;
    protected $table = 'price_privilege';
    protected $fillable = ['price_id', 'privilege_id'];

    public function price()
    {
        return $this->belongsTo(Price::class, 'price_id');
    }

    public function privilege()
    {
        return $this->belongsTo(Privilege::class, 'privilege_id');
    }
}
