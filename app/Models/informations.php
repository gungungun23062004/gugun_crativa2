<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class informations extends Model
{
    use HasFactory;
    protected $table = 'informations';
    protected $guarded = ['id'];
    protected $fillable = [
        'catalogue',
        'desc',
        'homeBannerImage',
        'quotes',
        'quotesBannerImage',
        'persuasive',
        'descPersuasive',
    ];
}
