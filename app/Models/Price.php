<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    use HasFactory;
    protected $fillable = ['type', 'price_per_month'];

    public function privileges()
    {
        return $this->belongsToMany(Privilege::class, 'price_privilege', 'price_id', 'privilege_id');
    }
}
