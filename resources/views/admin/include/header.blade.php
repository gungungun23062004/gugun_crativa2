<nav class="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme"
    id="layout-navbar">
    <div class="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
        <a class="nav-item nav-link px-0 me-xl-4" href="javascript:void(0)">
            <i class="bx bx-menu bx-sm"></i>
        </a>
    </div>

    <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
        <ul class="navbar-nav flex-row align-items-center ms-auto">
            <!-- Place this tag where you want the button to render. -->
            <li class="nav-item lh-1 me-3">
                <a class="github-button" href="" data-icon="octicon-star" data-size="large"
                    data-show-count="true"
                    aria-label="Star themeselection/sneat-html-admin-template-free on GitHub">Admin</a>
            </li>

            <!-- User -->
            <li class="nav-item navbar-dropdown dropdown-user dropdown">
                <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown">
                    <div class="avatar avatar-online">
                        <img src="../assets/img/avatars/1.png" alt class="w-px-40 h-auto rounded-circle" />
                    </div>
                </a>
                <ul class="dropdown-menu dropdown-menu-end">
                    <li>
                        <a class="dropdown-item" href="/">
                            <i class="menu-icon tf-icons bx bx-home-circle"></i>
                            <span class="align-middle">Homepage</span>
                        </a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="" onclick="confirmLogout()">
                            <i class="bx bx-power-off me-2"></i>
                            <span class="align-middle">Log Out</span>
                        </a>
                    </li>
                </ul>
                <script>
                    function confirmLogout() {
                        if (confirm("Are you sure you want to logout?")) {
                            // Jika pengguna menekan OK, lakukan logout
                            window.location.href = "/actionlogout"; // Sesuaikan dengan URL logout Anda
                        } else {
                            // Jika pengguna menekan Cancel, tidak melakukan apa-apa
                        }
                    }
                </script>
            </li>
            <!--/ User -->
        </ul>
    </div>
</nav>
