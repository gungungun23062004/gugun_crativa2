@extends('admin.layouts.master')
@section('content')

    <div class="content-wrapper">
        <!-- Content -->
        <div class="container-xxl flex-grow-1 container-p-y">
            <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Edit/</span>Junction</h4>

            <!-- Basic Layout & Basic with Icons -->
            <div class="row">
                <!-- Basic Layout -->
                <div class="col-xxl">
                    <div class="card mb-4">
                        <div class="card-header d-flex align-items-center justify-content-between">
                        </div>
                        <div class="card-body">
                            <form action="{{ route('priceprivilege.update', $priceprivilege->id) }}" method="POST">
                                @csrf
                                @method('PUT')

                                <div class="mb-3">
                                    <label for="type-price" class="form-label">Type Price</label>
                                    <select class="form-select" id="type-price" aria-label="Default select example"
                                        name="type">
                                        {{-- <option selected="">{{$priceprivilege->price->type}}</option> --}}
                                        @foreach ($price as $item)
                                            <option value="{{ $item->id }}">{{ $item->type }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="mb-3">
                                    <label for="privilege" class="form-label">Privilege</label>
                                    <select class="form-select" id="privilege" aria-label="Default select example"
                                        name="privilege">
                                        {{-- <option selected="">{{$priceprivilege->privilege->privilege}}</option> --}}
                                        @foreach ($privilege as $item)
                                            <option value="{{ $item->id }}">{{ $item->privilege }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="row justify-content-end">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Edit</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- / Content -->

        <!-- Footer -->
        @include('admin.include.footer')
        <!-- / Footer -->

        <div class="content-backdrop fade"></div>
    </div>

@stop
