@extends('admin.layouts.master')
@section('content')

    <div class="content-wrapper">
        <!-- Content -->
        <div class="container-xxl flex-grow-1 container-p-y">
            <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Add/ Price</span></h4>

            <!-- Basic Layout & Basic with Icons -->
            <div class="row">
                <!-- Basic Layout -->
                <div class="col-xxl">
                    <div class="card mb-4">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <h5 class="mb-0">Price new</h5>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('price.store') }}" method="POST">
                                @csrf

                                <div class="row mb-3">
                                    <label class="col-sm-2 col-form-label" for="type">Type</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="type" name="type" required>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label class="col-sm-2 col-form-label" for="price_per_month">Price per Month</label>
                                    <div class="col-sm-10">
                                        <textarea id="price_per_month" name="price_per_month" class="form-control" required></textarea>
                                    </div>
                                </div>

                                <div class="row justify-content-end">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Create Price</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- / Content -->

        <!-- Footer -->
        @include('admin.include.footer')
        <!-- / Footer -->

        <div class="content-backdrop fade"></div>
    </div>

@stop
