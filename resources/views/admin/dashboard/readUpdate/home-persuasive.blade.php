@extends('admin.layouts.master')
@section('content')

    <div class="content-wrapper">
        <!-- Content -->

        <div class="container-xxl flex-grow-1 container-p-y">
            <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Read update/</span>Persusaive Homepage</h4>

            <!-- Basic Layout & Basic with Icons -->
            <div class="row">
                <!-- Basic Layout -->
                <div class="col-xxl">
                    <div class="card mb-4">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <h5 class="mb-0">Persuasive Homepage Now</h5>
                        </div>
                        <div class="card-body">
                            @foreach ($informations as $info)
                                <form action="{{ route('update.persuasive', $info->id) }}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <div class="row mb-3">
                                        <label class="col-sm-2 col-form-label" for="persuasive">Persuasive</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="persuasive" name="persuasive"
                                                value=" {{ $info->persuasive }}">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-sm-2 col-form-label" for="descPersuasive">Description
                                            Persuasive</label>
                                        <div class="col-sm-10">
                                            <textarea id="descPersuasive" name="descPersuasive" class="form-control">{{ $info->descPersuasive }}</textarea>
                                        </div>
                                    </div>
                                    <div class="row justify-content-end">
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary">Send</button>
                                        </div>
                                    </div>
                                </form>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- / Content -->

        <!-- Footer -->
        @include('admin.include.footer')
        <!-- / Footer -->

        <div class="content-backdrop fade"></div>
    </div>

@stop
