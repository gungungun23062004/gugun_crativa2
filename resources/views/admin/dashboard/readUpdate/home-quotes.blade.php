@extends('admin.layouts.master')
@section('content')

    <div class="content-wrapper">
        <!-- Content -->
        <div class="container-xxl flex-grow-1 container-p-y">
            <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Read update/</span>Quotes Homepage</h4>

            <!-- Basic Layout & Basic with Icons -->
            <div class="row">
                <!-- Basic Layout -->
                <div class="col-xxl">
                    <div class="col-sm-4 col-lg-12 mb-4">
                        <div class="card bg-primary text-white text-center p-3">
                            <figure class="mb-0">
                                @foreach ($informations as $info)
                                    <blockquote class="blockquote">
                                        <p style="font-size: 30px;">"{{ $info->quotes }}"</p>
                                    </blockquote>
                                @endforeach
                            </figure>
                        </div>
                    </div>
                    <div class="card mb-4">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <h5 class="mb-0">Quotes Now</h5>
                        </div>

                        <div class="card-body">
                            @foreach ($informations as $info)
                                <form action="{{ route('update.quotes', $info->id) }}" method="POST"
                                    enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="row mb-3">
                                        <label class="col-sm-2 col-form-label" for="quotes">Quotes</label>
                                        <div class="col-sm-10">
                                            <textarea id="quotes" name="quotes" class="form-control">{{ $info->quotes }}</textarea>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Choice Picture</label>
                                        <input class="form-control" type="file" id="formFile" name="imageQuote">
                                    </div>
                                    <div class="row justify-content-end">
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary">Send</button>
                                        </div>
                                    </div>
                                </form>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- / Content -->

        <!-- Footer -->
        @include('admin.include.footer')
        <!-- / Footer -->

        <div class="content-backdrop fade"></div>
    </div>

@stop
