@extends('admin.layouts.master')
@section('content')

    <div class="content-wrapper">
        <!-- Content -->
        <div class="container-xxl flex-grow-1 container-p-y">
            <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Read update/</span>Homepage banner</h4>

            <!-- Basic Layout & Basic with Icons -->
            <div class="row">
                <!-- Basic Layout -->
                <div class="col-xxl">
                    <div class="card mb-4">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <h5 class="mb-0">Homepage Banner Now</h5>
                        </div>
                        <div class="card-body">
                            @foreach ($informations as $info)
                                <form action="{{ route('update.homebanner', $info->id) }}" method="POST"
                                    enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="row mb-3">
                                        <label class="col-sm-2 col-form-label" for="catalogue">Catalogue</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="catalogue" name="catalogue"
                                                value="{{ $info->catalogue }}" placeholder="">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label class="col-sm-2 col-form-label" for="desc">Description Catalogue</label>
                                        <div class="col-sm-10">
                                            <textarea id="desc" name="desc" class="form-control">{{ $info->desc }}</textarea>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Choice Picture</label>
                                        <input class="form-control" type="file" id="formFile" name="image">
                                    </div>
                                    <div class="row justify-content-end">
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary">Send</button>
                                        </div>
                                    </div>
                                </form>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- / Content -->

        <!-- Footer -->
        @include('admin.include.footer')
        <!-- / Footer -->

        <div class="content-backdrop fade"></div>
    </div>

@stop
