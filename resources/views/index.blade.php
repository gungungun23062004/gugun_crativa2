<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Fonts Google - Lexend -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+SC:wght@300;400;500&display=swap" rel="stylesheet">
    <!-- Icofont -->
    <link rel="stylesheet" href="{{ asset('assets/css/icofont.min.css') }}">
    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}">
    <!-- Title -->
    <title>Crativa Template</title>
</head>

<body>
    <!-- Navigation -->
    <div class="nav-top px-lg-4 px-2">
        <div class="nav-inner h-100">
            <div class="container-fluid h-100">
                <div class="row h-100">
                    <div class="col-6 h-100">
                        <div class="d-flex align-items-center h-100">
                            <div class="me-lg-4 me-3">
                                <a href="#" class="nav-logo">
                                    Crativa
                                </a>
                            </div>
                            <div class="">
                                <div class="nav-top-menus">
                                    <a href="#featuresSection" class="menus-link me-3">Features</a>
                                    <a href="#pricingSection" class="menus-link me-3">Pricing</a>
                                    <a href="#contactSection" class="menus-link">Contact</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 h-100">
                        <div class="d-flex align-items-center justify-content-end h-100">
                            <div class="nav-top-menus">
                                <a href="#" class="menus-link me-3"><i class="icofont-instagram"></i></a>
                                <a href="#" class="menus-link"><i class="icofont-twitter"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Section > Home Banner -->
    <div class="d-block py-lg-5 py-3">
        <div class="container my-lg-5 my-4 px-lg-5">
            <div class="row align-items-center">
                <div class="col-md-7 mb-lg-5 mb-4">
                    <h1 class="mb-3 text-xlg">{{ $information->catalogue }}</h1>
                    <p class="mb-4 font-regular pe-lg-5">{{ $information->desc }}</p>
                    @if (Auth::check())
                        <a href="{{ route('dashboard') }}" class="btn btn-primary btn-lg px-4">Dashboard <i
                                class="icofont-arrow-right ms-3"></i></a>
                    @else
                        <a href="/signUp" class="btn btn-primary btn-lg px-4">Signup Now <i
                                class="icofont-arrow-right ms-3"></i></a>
                    @endif

                </div>
                <div class="col-md-5">
                    <div class="d-block block-img-tall rounded"
                        style="background-image: url('{{ asset('assets/img/home/' . $information->homeBannerImage) }}')">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Section > Feature -->
    <div id="featuresSection" class="d-block bg-light py-lg-5 py-3">
        <div class="container my-lg-5 my-4 px-lg-5">
            <h2 class="mb-lg-5 mb-4">Features</h2>
            <div class="row">
                @foreach ($features as $item)
                    <div class="col-md-4 mb-lg-0 mb-3">
                        <div class="d-flex flex-column pb-3 mb-3 border-bottom border-primary h-100">
                            <div class="text-lg font-thin text-primary d-block mb-3">{{ $loop->iteration }}</div>
                            <div class="flex-1">
                                <h4 class="mb-3">{{ $item->feature }}</h4>
                                <p class="small">{{ $item->desc }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- Section -->
    <div class="d-block bg-primary py-lg-5 py-3">
        <div class="container my-lg-5 my-4 px-lg-5">
            <div class="d-block text-lg font-thin mb-4">
                {{ $information->quotes }}
            </div>
        </div>
    </div>
    <div class="d-block section-xlg bg-images"
        style="background-image: url('{{ asset('assets/img/home/' . $information->quotesBannerImage) }}')"></div>
    <!-- Section > Pricing -->
    <div id="pricingSection" class="d-block bg-light py-lg-5 py-3">
        <div class="container my-lg-5 my-4 px-lg-5">
            <h2 class="mb-lg-5 mb-4">Pricing</h2>
            <div class="row">
                {{-- Loop through data from the 'price' table --}}
                @foreach ($prices as $price)
                    <div class="col-md-4 mb-lg-0 mb-4">
                        <div class="card h-100 border-0">
                            <div class="card-body">
                                <div class="d-flex flex-column p-lg-3 h-100">
                                    <div class="flex-1 mb-4">
                                        <h5>{{ $price->type }}</h5>
                                        <div class="d-block text-lg font-thin">
                                            $ {{ $price->price_per_month }} /month
                                        </div>

                                        {{-- Check if there are associated privileges --}}
                                        @if ($price->privileges->isNotEmpty())
                                            {{-- Loop through privileges related to the current price --}}
                                            <ul class="list-group list-group-flush">
                                                @foreach ($price->privileges as $privilege)
                                                    <li class="list-group-item border-bottom px-0">
                                                        {{ $privilege->privilege }}</li>
                                                @endforeach
                                            </ul>
                                        @else
                                            <p>No privileges available.</p>
                                        @endif
                                    </div>

                                    <div class="d-block w-100 text-center">
                                        <a href="#" class="btn btn-primary d-block mb-2">Choose This Plan</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
    <div class="d-block bg-light py-lg-5 py-3">
        <div class="container my-lg-5 my-4 px-lg-5">
            <div class="row justify-content-center">
                <div class="col-md-7">
                    <div class="d-block text-center">
                        <h1 class="mb-3">{{ $information->persuasive }}</h1>
                        <p class="font-regular">{{ $information->descPersuasive }}</p>
                        @if (Auth::check())
                            <a href="{{ route('dashboard') }}" class="btn btn-primary btn-lg px-4">Dashboard <i
                                    class="icofont-arrow-right ms-3"></i></a>
                        @else
                            <a href="/signUp" class="btn btn-primary btn-lg px-4">Signup Now <i
                                    class="icofont-arrow-right ms-3"></i></a>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="d-block bg-primary py-lg-5 py-3">
        <div class="container px-lg-5">
            <div class="d-flex d-flex-mobile align-items-start justify-content-center">
                <div class="mx-3 text-center mb-lg-0 mb-3">
                    <small class="font-weight-bold"><i class="icofont-phone me-2"></i>Phone</small>
                    <div class="d-block font-weight-bold">+62 {{ $contacts->phone }}</div>
                </div>
                <div class="mx-3 text-center mb-lg-0 mb-3">
                    <small class="font-weight-bold"><i class="icofont-envelope me-2"></i>Email</small>
                    <div class="d-block font-weight-bold">{{ $contacts->email }}</div>
                </div>
                <div class="mx-3 text-center mb-lg-0 mb-3">
                    <small class="font-weight-bold"><i class="icofont-location-pin me-2"></i>Location</small>
                    <div class="d-block font-weight-bold">{{ $contacts->location }}</div>
                </div>
            </div>
        </div>
    </div>
    <!-- Navigation -->
    <div class="nav-top px-lg-4 px-2 pb-5 pt-5">
        <div class="nav-inner h-100">
            <div class="container-fluid h-100">
                <div class="row h-100">
                    <div class="col-6 h-100">
                        <div class="d-flex align-items-center h-100">
                            <div class="me-lg-4 me-3">
                                <a href="#" class="nav-logo">
                                    Crativa
                                </a>
                            </div>
                            <div class="">
                                <div class="nav-top-menus">
                                    <a href="#featuresSection" class="menus-link me-3">Features</a>
                                    <a href="#pricingSection" class="menus-link me-3">Pricing</a>
                                    <a href="#contactSection" class="menus-link">Contact</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 h-100">
                        <div class="d-flex align-items-center justify-content-end h-100">
                            <div class="nav-top-menus">
                                <a href="#" class="menus-link me-3"><i class="icofont-instagram"></i></a>
                                <a href="#" class="menus-link"><i class="icofont-twitter"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
