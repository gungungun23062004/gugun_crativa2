<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\InformationsController;
use App\Models\informations;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'getIndexView'])->name('index.view');

Route::get('/signUp', [LoginController::class, 'signup']);
Route::post('actionlogin', [LoginController::class, 'actionlogin'])->name('actionlogin');

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard')->middleware('auth');
Route::get('actionlogout', [LoginController::class, 'actionlogout'])->name('actionlogout')->middleware('auth');

Route::get('/dashboard/homebanner', [DashboardController::class, 'homebanner'])->name('homebanner')->middleware('auth');
Route::get('/dashboard/homequotes', [DashboardController::class, 'homequotes'])->name('homequotes')->middleware('auth');
Route::get('/dashboard/homepersuasive', [DashboardController::class, 'homepersuasive'])->name('homepersuasive')->middleware('auth');
Route::get('/dashboard/homecontacts', [DashboardController::class, 'homecontacts'])->name('homecontacts')->middleware('auth');

Route::get('/dashboard/features', [DashboardController::class, 'features'])->name('features')->middleware('auth');
Route::get('/features/create', [DashboardController::class, 'createFeatures'])->name('features.create')->middleware('auth');
Route::post('/dashboard/features/store', [DashboardController::class, 'storeFeature'])->name('features.store');
Route::get('/editFeature/{id}', [DashboardController::class, 'editFeatures'])->name('edit.feature')->middleware('auth');
Route::put('/dashboard/features/update/{id}', [DashboardController::class, 'updateFeature'])->name('update.features');
Route::delete('/dashboard/features/delete/{id}', [DashboardController::class, 'featuresdelete'])->name('features.delete')->middleware('auth');


Route::get('/dashboard/pricing', [DashboardController::class, 'pricing'])->name('pricing')->middleware('auth');
Route::get('/price/create', [DashboardController::class, 'createPrice'])->name('price.create')->middleware('auth');
Route::post('/dashboard/price/store', [DashboardController::class, 'storePrice'])->name('price.store');
Route::get('/editPrice/{id}', [DashboardController::class, 'editPrice'])->name('price.edit')->middleware('auth');
Route::put('/dashboard/price/update/{id}', [DashboardController::class, 'updatePrice'])->name('price.update');
Route::delete('/dashboard/price/delete/{id}', [DashboardController::class, 'deletePrice'])->name('price.delete')->middleware('auth');

Route::get('/privilege/create', [DashboardController::class, 'createPrivilege'])->name('privilege.create')->middleware('auth');
Route::post('/dashboard/privilege/store', [DashboardController::class, 'storePrivilege'])->name('privilege.store');
Route::get('/privilege/{id}', [DashboardController::class, 'editPrivilege'])->name('privilege.edit')->middleware('auth');
Route::put('/dashboard/privilege/update/{id}', [DashboardController::class, 'updatePrivilege'])->name('privilege.update');
Route::delete('/dashboard/privilege/delete/{id}', [DashboardController::class, 'deletePrivilege'])->name('privilege.delete')->middleware('auth');


Route::get('/priceprivilege/create', [DashboardController::class, 'createPricePrivilege'])->name('priceprivilege.create')->middleware('auth');
Route::post('/dashboard/priceprivilege/store', [DashboardController::class, 'storePricePrivilege'])->name('priceprivilege.store');
Route::get('/editPricePrivilege/{id}', [DashboardController::class, 'editPricePrivilege'])->name('priceprivilege.edit')->middleware('auth');
Route::put('/dashboard/priceprivilege/update/{id}', [DashboardController::class, 'updatePricePrivilege'])->name('priceprivilege.update');
Route::delete('/dashboard/priceprivilege/delete/{id}', [DashboardController::class, 'deletePricePrivilege'])->name('priceprivilege.delete')->middleware('auth');

Route::put('/homebanner/{id}', [InformationsController::class, 'updateHomeBanner'])->name('update.homebanner');
Route::put('/quotes/{id}', [InformationsController::class, 'updateQuotes'])->name('update.quotes');
Route::put('/persuasive/{id}', [InformationsController::class, 'updatePersuasive'])->name('update.persuasive');
Route::put('/contacts/{id}', [InformationsController::class, 'updateContacts'])->name('update.contacts');
